'''Copyright 2015 Dacio "FireClawGames" Romero'''

#Imports modules
import pygame
import math
import logging
from random import randint
from random import random
from time import time

#Initializes pygame engine
pygame.init()

#Starts logs
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename='logs.log', level=logging.DEBUG)
logging.info("---New session started---")

# Define some colors
BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)
GREEN    = (   0, 255,   0)
RED      = ( 255,   0,   0)
BLUE     = (   0,   0, 255)
YELLOW   = ( 255, 255,   0)
SKY_BLUE = ( 135, 205, 250)

#Sets up screen
SCREEN_SIZE = (800, 800)
screen = pygame.display.set_mode(SCREEN_SIZE)
pygame.display.set_caption("Car Game Confirmed")
icon = pygame.image.load("icon_alpha.png")
pygame.display.set_icon(icon)

#Rotation function from: http://www.pygame.org/wiki/RotateCenter
def rot_center(image, angle):
    """rotate an image while keeping its center and size"""
    orig_rect = image.get_rect()
    rot_image = pygame.transform.rotate(image, angle).convert_alpha()
    rot_rect = orig_rect.copy()
    rot_rect.center = rot_image.get_rect().center
    rot_image = rot_image.subsurface(rot_rect).copy().convert_alpha()
    return rot_image

#<--- Defines classes --->
#Defines PhysObj class !FOR GENERAL USE!
class PhysObj():
    #Initializes PhysObj class
    def __init__(self, posX, posY):
        self.posX = posX
        self.posY = posY
        self.goToDone = True
        self.rateGot = False
    #Move function
    def move(self, xChange, yChange):
        self.posX += xChange
        self.posY += yChange
    #Goto function
    def goTo(self, xCoord, yCoord, time):
        if not self.rateGot:
            #Gets rate in order to cause the object reach it at the correct time
            self.xRate = (xCoord - self.posX) / time
            self.yRate = (yCoord - self.posY) / time
            #Causes if statement to not be run until it reaches its destination
            self.goToDone = False
            self.rateGot = True
        if xCoord == int(self.posX) or yCoord == int(self.posY):
            #Resets variables for next call
            self.goToDone = True
            self.rateGot = False
            #Sets object's position to exactly yo destination
            self.posX = xCoord
            self.posY = yCoord
        #Moves object if not at destination
        else:
            self.move(self.xRate, self.yRate)

#Defines Worm class
class Worm(PhysObj):
    #Initializes worm class
    def __init__(self, posX, posY):
        PhysObj.__init__(self, posX, posY)
        self.animPos = 0
    #Animates worm
    def anim(self):
        if self.animPos == 83: #Resets animation to beginning
            self.animPos = 0
        self.animPos += 1 #Steps animation position up
        screen.blit(pygame.image.load('worm/worm' + str(self.animPos // 4) + '.png'), [self.posX, self.posY]) #Loads worm'

#Defines Bird class
class Bird(PhysObj):
    #Initializes Bird class !BIRD IMAGES MUST FACE LEFT!
    def __init__(self, posX, posY, birdOpen, birdClosed):
        PhysObj.__init__(self, posX, posY)
        self.birdClosed = pygame.image.load(birdClosed).convert_alpha()
        self.birdOpen = pygame.image.load(birdOpen).convert_alpha()
        self.currentBird = self.birdOpen
        self.otherBird = self.birdClosed
        #Defines variables for following functions
        self.flapCount = 0
        self.flipped = False
    #Defines flyTo function !USES goTo!
    def flyTo(self, xCoord, yCoord, time):
        self.goTo(xCoord, yCoord, time)
        #Flaps bird's wings
        if self.flapCount % 10 == 0:
            self.flapToggle()
        self.flapCount += 1
        #Makes bird face correct direction
        if self.xRate > 0:
            self.flipped = True
        elif self.xRate < 0:
            self.flipped = False
        #Resets varaibles
        if self.goToDone:
            self.flapCount = 0
            self.currentBird = self.birdClosed
            self.otherBird = self.birdOpen
    #Defines flapToogle function
    def flapToggle(self):
        self.currentBird, self.otherBird = self.otherBird, self.currentBird
    #Defines render function
    def render(self):
        #Flips bird if flyTo defined it as True
        if not self.flipped:
            screen.blit(self.currentBird, [self.posX, self.posY])
        else:
            screen.blit(pygame.transform.flip(self.currentBird, True, False), [self.posX, self.posY])

#Defines Vehicle class
class Vehicle(PhysObj):
    #Initializes Vehicle class
    def __init__(self, posX, posY, car, light, lightOffset, horn):
        PhysObj.__init__(self, posX, posY)
        self.vehicle = pygame.image.load(car).convert_alpha()
        self.vehicleFlipped = pygame.transform.flip(self.vehicle, True, False)
        self.vehicleWidth = self.vehicle.get_width()
        self.vehicleHeight = self.vehicle.get_height()
        self.light = pygame.image.load(light).convert_alpha()
        self.lightFlipped = pygame.transform.flip(self.light, True, False)
        self.lightOffset = lightOffset
        self.flipped = False
        self.horn = pygame.mixer.Sound(horn)
        self.horn.set_volume(.25)
    #Defines drive function !USES move!
    def drive(self, xChange, yChange):
        #Idles car if not moving
        if xChange == 0 and yChange == 0:
            self.rumbleY = self.posY + randint(-1, 1) #Rumbles vehicle over 3 units
        else:
            self.rumbleY = self.posY + randint(0, 1) #Rumbles vechicle over 2 units
        if xChange < 0:
            self.flipped = True
            self.lightX = self.posX + 86
        elif xChange > 0:
            self.flipped = False
        else:
            self.idle = True
        #Moves vehicle forward
        self.move(xChange, yChange)
        #Teleports vehicle if it is offscreen
        if self.posX < -self.vehicle.get_width() and xChange < 0:
            self.posX = SCREEN_SIZE[0] + self.lightOffset[0] + self.light.get_width()
        if self.posX > SCREEN_SIZE[0] and xChange > 0:
            self.posX = -147
    #Defines render function
    def render(self):
        #Rumbles vehicle if idle
        if self.flipped:
            screen.blit(self.vehicleFlipped, [self.posX, self.rumbleY])
        else:
            screen.blit(self.vehicle, [self.posX, self.rumbleY])
    #Defines renderLights function
    def renderLights(self):
        #Renders lights if alpha of sunLight is low
        if sunLight.get_alpha() > 127.5:
            if self.flipped:
                screen.blit(self.lightFlipped, [((self.light.get_width() - self.posX) - (self.vehicle.get_width() - self.lightOffset[0])) * -1, self.rumbleY + self.lightOffset[1]])
            else:
                screen.blit(self.light, [self.posX + self.lightOffset[0], self.rumbleY + self.lightOffset[1]])
    #Defines honk function
    def honk(self):
        self.horn.play()
#Defines Car class !NOTHING SPECIAL YET!
class Car(Vehicle):
    def __init__(self, posX, posY, car, light, lightOffset, horn):
        Vehicle.__init__(self, posX, posY, car, light, lightOffset, horn)

#Defines some variables
sunPos = 0
moonPos = 160
sunX = int(math.cos(sunPos) * 200 + 400)
sunY = int(math.sin(sunPos) * 200 + 400)
moonX = int(math.cos(moonPos) * 300 + 400)
moonY = int(math.sin(moonPos) * 300 + 400)
musicDone = True
countBirds = 0
event1 = event2 = False
speed = 1
starBlanketRotation = 0

#Creates images and light surface
sunLight = pygame.Surface(SCREEN_SIZE)
sunLight.fill(BLACK)
grass = pygame.Surface([SCREEN_SIZE[0], SCREEN_SIZE[1] / 2])
grass.fill(GREEN)
for i in range(0, 401, 2):
    for j in range(0, 801, 2):
        pygame.draw.rect(grass, [randint(0, 75), randint(150, int(191.25)), randint(0, 75)], [j, i, 2, 2])
tree = pygame.image.load("tree.png").convert_alpha()
starBlanketSize = int(math.sqrt(SCREEN_SIZE[0] ** 2 + SCREEN_SIZE[1] ** 2))
starBlanket = pygame.Surface([starBlanketSize, starBlanketSize], pygame.SRCALPHA, 32)
for i in range(75):
    pygame.draw.circle(starBlanket, WHITE, [randint(0, starBlanketSize), randint(0, starBlanketSize)], randint(2, 4))

#Defines objects
player = Car(302, 380, 'car/car.png', 'car/light.png', [92, -1], 'car/horn.ogg')
bird1 = Bird(-25, -25, 'bird/birdOpen.png', 'bird/birdClosed.png')
worm1 = Worm(400, 397)
    
#Loads sounds
songs = ["music1.ogg", "music2.ogg", "music3.ogg", "music4.ogg", "music5.ogg", "music6.ogg"]
songToPlay = None
songsPlayed = []

done = False
clock = pygame.time.Clock()
#Main loop
while not done:
    #User input loop
    for event in pygame.event.get():
        #Quits program when X is clicked
        if event.type == pygame.QUIT:
            done = True
        
        #Car movement true
        if event.type == pygame.KEYDOWN:
            #AIRHOOOOOOORRRNNN
            if event.key == pygame.K_SPACE:
                player.honk()
    keys = pygame.key.get_pressed()
    if not((keys[pygame.K_RIGHT] or keys[pygame.K_d]) and (keys[pygame.K_LEFT] or keys[pygame.K_a])):
        #Car movement right
        if keys[pygame.K_RIGHT] or keys[pygame.K_d]:
            player.drive(5, 0)
        #Car movement left
        elif keys[pygame.K_LEFT] or keys[pygame.K_a]:
            player.drive(-5, 0)
        #Car idle
        else:
            player.drive(0, 0)
    #Changes the speed of the sun up
    if keys[pygame.K_UP]:
        speed += 1
    #Changes the speed of the sun down
    elif keys[pygame.K_DOWN]:
        speed -= 1
    
    #<--- Game music --->
    if not pygame.mixer.music.get_busy(): #Plays music if music is not playing
        logging.info('Generating new songToPlay.')
        songToPlay = songs[randint(0, len(songs) - 1)]
        logging.info('Generated ' + str(songToPlay))
        while songToPlay in songsPlayed:
            logging.info(str(songToPlay) + ' is in songsPlayed!')
            logging.info('Generating new songToPlay.')
            songToPlay = songs[randint(0, len(songs) - 1)]
            logging.info('Generated ' + str(songToPlay))
        logging.info(str(songToPlay) + ' is not in songsPlayed.')
        logging.info('Loading songToPlay')
        pygame.mixer.music.load('music/' + songToPlay)
        logging.info('Playing songToPlay')
        pygame.mixer.music.play()
        if len(songsPlayed) > 2:
            logging.info('songsPlayed is greater than 4!')
            logging.info('Removing ' + str(songsPlayed[0]) + ' from songsPlayed')
            songsPlayed.remove(songsPlayed[0])
        logging.info('Appending ' + str(songToPlay) + ' to songsPlayed')
        songsPlayed.append(songToPlay)
    
    #<--- Game logic --->
    #Sets sun coordinates based on degrees of a circle in radians located at 400, 400 and a radius of 200 
    sunX = int(math.cos(sunPos) * 200 + 400)
    sunY = int(math.sin(sunPos) * 200 + 400)
    
    #Sets sun's position in radians
    if sunPos != 0:
        sunPos += .05 * (math.pi / 180) * speed
    else:
        sunPos = .05 * (math.pi / 180) * speed
    
    #Sets sun coordinates based on degrees of a circle in radians located at 400, 400 and a radius of 200 
    moonX = int(math.cos(moonPos) * 300 + 400)
    moonY = int(math.sin(moonPos) * 300 + 400)
    
    #Sets sun's position in radians
    if moonPos != 0:
        moonPos += .05 * (math.pi / 180) * speed
    else:
        moonPos = .05 * (math.pi / 180) * speed
    
    starBlanketRotation = -sunPos / 1.5 * 180 / math.pi
    starBlanketRotated = rot_center(starBlanket, starBlanketRotation)
    starBlanketAlpha = int(255 * ((sunY - 200) / 400)) - 30
    if starBlanketAlpha < 0:
        starBlanketAlpha = 0
    
    #Sets sunlight transparency based on sun elevation and renders
    sunLight.set_alpha(int(255 * ((sunY - 200) / 400)) - 20)
    
    #<--- Game renders --->
    
    #Sets background
    screen.fill(SKY_BLUE)
    
    #Displays stars with alpha
    starBlanketRotated.fill((255, 255, 255, starBlanketAlpha), None, pygame.BLEND_RGBA_MULT)
    screen.blit(starBlanketRotated, [-(starBlanketSize - SCREEN_SIZE[0]) / 2, -(starBlanketSize - SCREEN_SIZE[1]) / 2])
    
    #Draws sun and moon
    pygame.draw.circle(screen, YELLOW, [sunX, sunY], 40)
    pygame.draw.circle(screen, WHITE, [moonX, moonY], 20)
    
    #Draws pixelated staticy grass
    screen.blit(grass, [0, 400])
    
    #Renders random worm
    worm1.anim()
    
    #Renders player
    player.render()
    
    
    #Renders player's lights
    player.renderLights()
    
    #Renders 4 trees
    for i in range(0, 801, 200):
        screen.blit(tree, [i, 220])
    
    #Moves bird
    if not event1:
        bird1.flyTo(100, 250, 200)
        event1 = bird1.goToDone
    #Renders bird
    bird1.render()
    
    #Renders sun light
    screen.blit(sunLight, [0, 0])
    
    #Displays renders
    pygame.display.flip()
    clock.tick(60)

#Stops pygame
pygame.quit()